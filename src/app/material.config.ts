import { Validators } from '@angular/forms';

export const MATERIAL_CONFIG = {
    moduleLabel: 'MATERIAL.TITLE',
    moduleDescription: 'MATERIAL.DESCRIPTION',
    search: {
        tblName: 'product-material',
        fields: [
            [
                {
                    label: 'MATERIAL.FIELDS.PLANT',
                    name: 'LOCID',
                    value: '',
                    valueKey: 'LOCID',
                    childKey: 'MATID',
                    childWhere: ['LOCID'],
                    parentKeys: [],
                    type: 'input',
                    inputType: 'text',
                    option: '',
                    validations: [
                        { name: "required", validator: Validators.required, message: "MAT_NAME Required" }
                    ]
                },
                {
                    label: 'MATERIAL.FIELDS.MATNO',
                    name: 'MATID',
                    value: '',
                    valueKey: 'MATID',
                    childKey: 'ALTBOM',
                    childWhere: ['LOCID', 'MATID'],
                    parentKeys: ['LOCID'],
                    type: 'input',
                    inputType: 'text',
                    option: '',
                    validations: [
                        { name: "required", validator: Validators.required, message: "MAT_NAME Required" }
                    ]
                },
                {
                    label: 'MATERIAL.FIELDS.ALTBOM',
                    name: 'ALTBOM',
                    value: '',
                    valueKey: 'ALTBOM',
                    childKey: '',
                    childWhere: [],
                    parentKeys: ['LOCID', 'MATID'],
                    type: 'input',
                    inputType: 'text',
                    option: '',
                    validations: [
                        { name: "required", validator: Validators.required, message: "MAT_NAME Required" }
                    ]
                }
            ]
        ]
    }
};
