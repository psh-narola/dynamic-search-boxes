import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ComponentService } from 'src/app/component.service';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
    selector: 'app-search-boxes',
    templateUrl: './search-boxes.component.html',
    styles: []
})
export class SearchBoxesComponent implements OnInit {

    @Input() searchBoxConfig;
    searchBoxGroup: FormGroup;
    typeaheadObj = {};
    searchBoxOptions = {};
    apiReqObj = {};
    @Output() searchCriteria: EventEmitter<any> = new EventEmitter<any>();

    constructor(
        private formBuilder: FormBuilder,
        private componentService: ComponentService
    ) { }

    ngOnInit(): void {
        this.fillDropDown(this.createReqObj(this.searchBoxConfig.fields[0][0]));
        this.createSearchBoxes();
    }

    fillDropDown(reqObj) {
        console.log('API request object = ', reqObj);
        this.searchBoxOptions[reqObj.column] = [
            'PSH',
            'VKO',
            'LRK',
            'MJA',
            'KAB',
            'KS'
        ]
    }

    createSearchBoxes() {
        this.searchBoxGroup = this.componentService.createFormFields(
            this.searchBoxConfig.fields,
            {},
            this.formBuilder.group({})
        );
        this.searchBoxConfig.fields.forEach(searchFieldRow => {
            searchFieldRow.forEach(searchField => {
                if (!this.searchBoxOptions.hasOwnProperty(searchField.name)) {
                    this.searchBoxOptions[searchField.name] = [];
                }
                this.typeaheadObj[searchField.name] = (text$: Observable<string>) => text$.pipe(
                    debounceTime(200),
                    distinctUntilChanged(),
                    map(term => term.length < 1 ? []
                        : this.searchBoxOptions[searchField.name].filter(v => v.toLowerCase().startsWith(term.toLocaleLowerCase())).splice(0, 10))
                );
                if (!this.apiReqObj.hasOwnProperty(searchField.name)) {
                    this.apiReqObj[searchField.name] = this.createReqObj(searchField);
                }
            });
        });
    }

    selectedItem(item, fieldObj) {
        this.searchBoxGroup.value[fieldObj['name']] = item.item;
        this.resetChildTypehead(fieldObj['name']);
        if (fieldObj['childKey'] != '') {
            const reqObj = JSON.parse(JSON.stringify(this.apiReqObj[fieldObj['childKey']]));
            reqObj.where.forEach(conditionObj => {
                conditionObj.value = this.searchBoxGroup.value[conditionObj.columnName];
            });
            this.fillDropDown(reqObj);
        }
    }

    resetChildTypehead(fieldKey) {
        const searchBoxes = Object.keys(this.searchBoxOptions);
        const fieldIndex = searchBoxes.indexOf(fieldKey);
        const childTypeheads = searchBoxes.slice(fieldIndex + 1);
        const formValue = this.searchBoxGroup.value;
        childTypeheads.forEach(childTypehead => {
            formValue[childTypehead] = '';
            this.searchBoxGroup.setValue(formValue);
            this.searchBoxOptions[childTypehead] = [];
        });
    }

    createReqObj(searchField) {
        const reqObj = {
            tblName: this.searchBoxConfig.tblName,
            column: searchField.name,
            where: []
        };
        searchField.parentKeys.forEach(parentKey => {
            reqObj.where.push({
                columnName: parentKey,
                value: this.searchBoxGroup.value[parentKey]
            });
        });
        return reqObj;
    }

    submit() {
        this.searchCriteria.emit(this.searchBoxGroup.value)
    }
}
