import { Component } from '@angular/core';
import { MATERIAL_CONFIG } from './material.config';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    materialConfig = MATERIAL_CONFIG;
    searchedCriteria = {};
    constructor() { }

    searchCriteria(searchedCriteria) {
        this.searchedCriteria = searchedCriteria;
    }
}
